FROM tensorflow/tensorflow:2.11.0-gpu

RUN apt-get update
RUN apt-get install -yqq ffmpeg libsm6 libxext6 
RUN python3 -m pip install --upgrade pip
RUN pip install opencv-python
RUN pip install numpy
