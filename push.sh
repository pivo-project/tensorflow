#!/bin/sh

echo "$2" | docker login --username "$1" --password-stdin registry.gitlab.com
docker build -t registry.gitlab.com/pivo-project/tensorflow:cpu -f ./cpu.Dockerfile .
docker push registry.gitlab.com/pivo-project/tensorflow:cpu

docker build -t registry.gitlab.com/pivo-project/tensorflow:gpu -f ./gpu.Dockerfile .
docker push registry.gitlab.com/pivo-project/tensorflow:gpu